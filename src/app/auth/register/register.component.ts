import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { RegisterPayload } from '../register-payload';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerFom : FormGroup;
  registerPayload : RegisterPayload

  constructor(private authService : AuthService) { }

  ngOnInit(): void {

    this.registerFom = new FormGroup({
      'username' : new FormControl(null, [Validators.required]),
      'email' : new FormControl(null, [Validators.required]),
      'password' : new FormControl(null, [Validators.required]),
      'confirmPassword' : new FormControl(null , [Validators.required])
    })

  }


  onRegister(){

    this.registerPayload.username = this.registerFom.get("username").value;
    this.registerPayload.email = this.registerFom.get("email").value;
    this.registerPayload.password = this.registerFom.get("password").value;
    this.registerPayload.confirmPassword = this.registerFom.get("confirmPassword").value;

    this.authService.registerUser(this.registerPayload).subscribe((data)=>{

      console.log(data);

    }, (error)=>{

      console.log(error);

    });

  }

}
