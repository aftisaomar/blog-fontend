import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterPayload } from './register-payload';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url: string =  "http://localhost:8080/";

  constructor(private httpClient : HttpClient) { }

  registerUser(registerPayload : RegisterPayload) : Observable<any>{

    return this.httpClient.post(this.url + "signup",registerPayload)

  }

}
